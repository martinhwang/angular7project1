﻿# AngularSrc

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## How to build install and use the project

1. Go to node.js website: https://nodejs.org/en/
2. Download the node.js
3. Open your terminal
4. Check node and npm version in the terminal
    * windnow 
     - c: node --version
     - c: npm --vesion
     
    * Unix/Mac
     - $ node --version
     - $ npm --version
5. Install angular
    * windnow 
     - c: npm install -g @angular/cli
    
    * Unix/Mac
     - $ npm install -g @angular/cli
6. Go to angular-src folder, if folder is in Desktop
    * window
     - c:\Users\[userName]\angulr-src
    * Unix
     - $ ~/Desktop/angular-src
7. Install module inside angular-src folder in terminal
    * window
     - c: npm install
    
    * Unix/Mac
     - $ npm install
     
8. Run project
    type 'ng serve' in terminal

    
## License

I am building this project not to make money but for practice. Why not share this project with others? I hope this will help others understand coding with Angular 7.

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
