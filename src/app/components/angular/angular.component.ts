import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validator, Validators} from '@angular/forms';

@Component({
  selector: 'app-angular',
  templateUrl: './angular.component.html',
  styleUrls: ['./angular.component.scss']
})
export class AngularComponent implements OnInit {
  messageClass;
  message;
  newPost = false;
  loadingNote = false;
  form;
  constructor(private formBuilder: FormBuilder) { this.createNewNoteForm(); }


  createNewNoteForm(){
    this.form = this.formBuilder.group({
      title:['', Validators.compose([
        Validators.required,
        Validators.maxLength(50),
        Validators.minLength(5),
      ])],
      body:['', Validators.compose([
        Validators.required,
        Validators.maxLength(500),
        Validators.minLength(5),
      ])]
    })
  }

  alphaNumericValidation(controls){
    const regExp = new RegExp(/^[a-zA-Z0-9 ]+$/);
    if(regExp.test(controls.value)){
      return null;
    }else{
      return {'alphaNumericValidation': true}
    }
  }

  newNoteForm(){
    this.newPost = true;
  }

  reloadNotes(){
    this.loadingNote = true;
    setTimeout(()=>{
      this.loadingNote = false;
    }, 4000)
  }

  
  ngOnInit() {
  }

}
